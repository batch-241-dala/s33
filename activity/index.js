fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET'
})
.then((response) => response.json())
.then(function(json) {
	let data = json
	let newData = data.map(function(post) {
		return `${post.title}`
	})

	console.log(newData)

})




fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'GET'
})
.then((response) => response.json())
.then((json) =>
	console.log(`The item '${json.title}' on the list has a status of '${json.completed}'`)
	)


fetch('https://jsonplaceholder.typicode.com/todos/', {
		method: 'POST',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"title": "Created To Do List Item",
			"completed": false
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"status": "Pending",
			"title": "Updated To Do List Item",
			"description": "To update the my to do list with a different data structure",
			"dateCompleted": "Pending"
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"status": "Complete",
			"dateCompleted": "07/09/21"
	})
})

.then((response) => response.json())
.then((json) => console.log(json))



fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'DELETE'
	})



