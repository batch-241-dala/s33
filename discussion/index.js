// [SECTION] JavaScript Synchronous vs Asynchronous
// Asynchronous - means that we can proceed to execute other statements, while time consuming codes is running in the background.

// Fetch() method returns a promise that resolves to a "response object"
// promise - is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));



fetch('https://jsonplaceholder.typicode.com/posts')


// .then() captures the "response object" and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));



fetch('https://jsonplaceholder.typicode.com/posts')
// "json()" - from the "response object" to convert the data retrieve into JSON format to be used in our application
.then(response => response.json())
// Print the converted JSON value from the "fetch" request
.then((json) => console.log(json));
// "promise chain" - using multiple .then()


// The "asyn" and "await"
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};

fetchData();

// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"title": "Create Post",
			"body": "Create Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Update a post
fetch('https://jsonplaceholder.typicode.com/posts/30', {
		method: 'PUT',
		headers: {
		'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"title": "update post",
			"body": "Update Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/30', {
		method: 'DELETE'
	})

.then((response) => response.json())
.then((json) => console.log(json));